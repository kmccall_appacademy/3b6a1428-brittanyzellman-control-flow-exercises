# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |ch| str.delete!(ch) if ch == ch.downcase }
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  index = str.length / 2
  if str.length % 2 == 0
    return str.slice(index - 1..index)
  else
    return str[index]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  counter = 0
  str.each_char { |ch| counter += 1 if VOWELS.include?(ch) }
  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = num
  if num == 1
    return 1
  else
    while num > 1
      product *= num - 1
      num -= 1
    end
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  arr.each { |word| string << (word + separator) }
  if separator == ""
    return string
  else
    return string[0...-1]
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  array_of_chars = str.chars
  array_of_chars.each_index do |idx|
    if idx.even?
      str[idx] = str[idx].downcase
    else
      str[idx] = str[idx].upcase
    end
  end
  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  array = str.split
  five_or_more = array.map! do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end
  five_or_more.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizz_buzz = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      fizz_buzz << "fizzbuzz"
    elsif num % 3 == 0
      fizz_buzz << "fizz"
    elsif num % 5 == 0
      fizz_buzz << "buzz"
    else
      fizz_buzz << num
    end
  end
  fizz_buzz
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr = arr.reverse
  arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  (2..num / 2).each do |n|
    if num % n == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  fctrs = []
  (1..num).each do |n|
    fctrs << n if num % n == 0
  end
  fctrs
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  primes = []
  factor_all = factors(num)
  factor_all.each { |n| primes << n if prime?(n) }
  primes
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |num|
    if num.even?
      evens << num
    else
      odds << num
    end
  end
  if evens.length == 1
    return evens[0]
  else
    return odds[0]
  end 
end
